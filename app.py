# -*- coding: utf-8 -*-
import json
import os
from datetime import date, timedelta

import dash
import dash_auth
import numpy as np
import pandas as pd
import plotly.express as px
from dash import dcc, html, callback
from dash.dash_table import DataTable, FormatTemplate
from dash.dependencies import Input, Output, State

from pages import (
    overview,
    userProfile,
    fundPerformance,
    # appUsage,
    shopping,
    search,
)
from util.dataframe_util import weekly_change_summary, count_new_users, count_new_inv_users, \
    update_investments, calc_fum_current, calc_fum_since_inception, calc_fum_trends, reload_all_data
from util.db_connection import connect_to_analytical_db

# load allowed user credentials
app_users = {}
with open('credential/credential.json') as f:
    credential = json.load(f)
    app_users = {u['username']: u['password'] for u in credential['app_users']}

# init Dash App
app = dash.Dash(
    __name__,
    meta_tags=[{"name": "viewport", "content": "width=device-width"}],
    suppress_callback_exceptions=True
)
app.title = "Tanggram Dashboard"
dash_auth.BasicAuth(app, app_users)

# get App server
server = app.server

# Describe the layout/ UI of the app
app.layout = html.Div(
    [dcc.Location(id="url", refresh=False), html.Div(id="page-content")]
)

##############
# Parameters
##############
DATA_REFRESH = True

today = date.today()
last_sunday_offset = today.weekday() + 1  # convert day format mon-sun=0-6 => sun-sat=0-6
last_sunday = today - timedelta(days=last_sunday_offset)
last_sunday_str = last_sunday.strftime('%Y-%m-%d')
last_sunday2 = today - timedelta(days=last_sunday_offset + 7)
last_sunday2_str = last_sunday2.strftime('%Y-%m-%d')
# define a cutoff date (use today for the latest data)
REPORTING = False
if REPORTING:
    cutoff_dt_str = (last_sunday + timedelta(days=1)).strftime('%Y-%m-%d')
else:
    cutoff_dt_str = today.strftime('%Y-%m-%d')

##############
# Load data
##############
if DATA_REFRESH or not os.path.exists('data/df_user_ss_d.csv'):
    # connect to DB
    try:
        db_engine = connect_to_analytical_db()
        assert db_engine.name == 'postgresql'
        print('Connection to database.')
    except IOError:
        print('DB connection failed!')
    # extract data and save to CSV
    reload_all_data(db_engine, cutoff_dt_str)

# user snapshot data
date_cols = ['user_created_ts', 'user_updated_ts', 'last_access_session_created_ts', 'id_check_created_ts',
             'id_check_updated_ts', 'id_check_file_first_created_ts', 'id_check_file_passed_ts',
             'first_inv_created_ts', 'seed_first_inv_created_ts', 'spark_first_inv_created_ts', 'groupbuy_first_pmt_ts',
             'groupbuy_last_pmt_ts']
df_user_ss_d = pd.read_csv("data/df_user_ss_d.csv", low_memory=False, parse_dates=date_cols)

# investment users
df_inv_users = df_user_ss_d[df_user_ss_d['first_inv_created_ts'].notnull()]

# investment transaction snapshot data
date_cols = ['inv_created_at', 'inv_updated_at', 'inv_start_dt', 'inv_close_dt', 'inv_tran_created_at',
             'inv_tran_updated_at']
df_inv_tran_ss_d = pd.read_csv("data/df_inv_tran_ss_d.csv", low_memory=False, parse_dates=date_cols)

# merchant store
date_cols = ['merchant_created_ts', 'merchant_updated_ts', 'store_created_ts', 'store_updated_ts']
df_merchant_store_ss_d = pd.read_csv("data/df_merchant_store_ss_d.csv", low_memory=False, parse_dates=date_cols)

# shopping transactions
date_cols = ['cart_created_ts', 'cart_updated_ts', 'order_created_ts', 'order_updated_ts', 'product_created_ts',
             'product_updated_ts', 'pmt_created_ts', 'pmt_updated_ts']
df_shop_tran_ss_d = pd.read_csv("data/df_shop_tran_ss_d.csv", low_memory=False, parse_dates=date_cols)

# TODO: get data refresh timestamp
DATA_REFRESH_TS = df_inv_tran_ss_d['inv_tran_updated_at'].max().date().strftime("%d/%m/%Y")

#################
# Calculation
#################

# weekly change summary
df_weekly_summary = weekly_change_summary(df_user_ss_d, df_inv_tran_ss_d)
# new registered user counts
df_reg_users_counts = count_new_users(df_user_ss_d, freq='W')
# investment user counts
df_inv_users_counts, df_inv_users_counts_seed, df_inv_users_counts_spark = count_new_inv_users(df_user_ss_d, freq='W')
# in-progress withdrawals and deposits
_, df_withdrawal_in_prog, df_partial_withdrawal_in_prog, df_deposit_in_prog = update_investments(df_user_ss_d,
                                                                                                 df_inv_tran_ss_d)
# FUM (current)
df_fum_curr = calc_fum_current(df_user_ss_d, df_withdrawal_in_prog, df_partial_withdrawal_in_prog, df_deposit_in_prog)
# FUM (since inception)
df_fum_si = calc_fum_since_inception(df_user_ss_d)
# FUM trends
df_fum_trend = calc_fum_trends(df_user_ss_d, df_inv_tran_ss_d, freq='D')

# investment user gender
df_inv_user_genders = df_inv_users[df_inv_users['gender'].isin(['M', 'F'])]. \
    groupby(['gender']). \
    agg({'user_id': 'count', 'total_bal': 'sum', 'total_deposit_amt': 'sum',
         'total_withdrawal_amt': 'sum'})
df_inv_user_genders['dw_ratio'] = df_inv_user_genders['total_deposit_amt'] / df_inv_user_genders['total_withdrawal_amt']
df_inv_user_genders['avg_deposit'] = df_inv_user_genders['total_deposit_amt'] / df_inv_user_genders['user_id']
df_inv_user_genders['avg_withdrawal'] = df_inv_user_genders['total_withdrawal_amt'] / df_inv_user_genders['user_id']
user_metric_map = {'user_id': 'User Count #', 'total_bal': 'Total Balance $', 'total_deposit_amt': 'Total Deposit $',
                   'total_withdrawal_amt': 'Total Withdrawal $', 'dw_ratio': 'Total D/W Ratio',
                   'avg_deposit': 'Avg Deposit $', 'avg_withdrawal': 'Avg Withdrawal $'}
df_inv_user_genders = df_inv_user_genders.rename(columns=user_metric_map)

# investment user location
df_inv_user_loc = df_inv_users[df_inv_users['state'].notnull()].groupby(['state']). \
    agg({'user_id': 'count', 'total_bal': 'sum', 'total_deposit_amt': 'sum',
         'total_withdrawal_amt': 'sum'})
df_inv_user_loc['dw_ratio'] = df_inv_user_loc['total_deposit_amt'] / df_inv_user_loc['total_withdrawal_amt']
df_inv_user_loc['avg_deposit'] = df_inv_user_loc['total_deposit_amt'] / df_inv_user_loc['user_id']
df_inv_user_loc['avg_withdrawal'] = df_inv_user_loc['total_withdrawal_amt'] / df_inv_user_loc['user_id']
df_inv_user_loc = df_inv_user_loc.rename(columns=user_metric_map)

# investment user balance bins
df_inv_users_copy = df_inv_users.copy()
df_inv_users_copy.loc[:, 'balance_bins'] = pd.cut(x=df_inv_users['total_bal'],
                                                  bins=[0, 5000, 10000, 50000, 100000, 200000, 500000, np.inf],
                                                  labels=['$0-5k', '$5-10k', '$10-50k', '$50-100k', '$100-200k',
                                                          '$200-500k', '$500k+'])
df_inv_user_bal = df_inv_users_copy[df_inv_users_copy['balance_bins'].notnull()]. \
    groupby(['balance_bins']). \
    agg({'user_id': 'count', 'total_bal': 'sum', 'total_deposit_amt': 'sum', 'total_withdrawal_amt': 'sum'})
df_inv_user_bal['dw_ratio'] = df_inv_user_bal['total_deposit_amt'] / df_inv_user_bal['total_withdrawal_amt']
df_inv_user_bal['avg_deposit'] = df_inv_user_bal['total_deposit_amt'] / df_inv_user_bal['user_id']
df_inv_user_bal['avg_withdrawal'] = df_inv_user_bal['total_withdrawal_amt'] / df_inv_user_bal['user_id']
df_inv_user_bal = df_inv_user_bal.rename(columns=user_metric_map)

# investment user deposit frequency bins
df_inv_users_copy = df_inv_users.copy()
df_inv_users_copy.loc[:, 'deposit_bins'] = pd.cut(x=df_inv_users['total_deposit_cnt'],
                                                  bins=[0, 1, 5, 10, np.inf],
                                                  labels=['once only', '2-5 times', '5-10 times', '10 times+'])
df_inv_user_inv_freq = df_inv_users_copy[df_inv_users_copy['deposit_bins'].notnull()]. \
    groupby(['deposit_bins']). \
    agg({'user_id': 'count', 'total_bal': 'sum', 'total_deposit_amt': 'sum', 'total_withdrawal_amt': 'sum'})
df_inv_user_inv_freq['dw_ratio'] = df_inv_user_inv_freq['total_deposit_amt'] / \
                                   df_inv_user_inv_freq['total_withdrawal_amt']
df_inv_user_inv_freq['avg_deposit'] = df_inv_user_inv_freq['total_deposit_amt'] / \
                                      df_inv_user_inv_freq['user_id']
df_inv_user_inv_freq['avg_withdrawal'] = df_inv_user_inv_freq['total_withdrawal_amt'] / \
                                         df_inv_user_inv_freq['user_id']
df_inv_user_inv_freq = df_inv_user_inv_freq.rename(columns=user_metric_map)

# shopping payment by store
df_shopping_pmt_by_store = df_merchant_store_ss_d. \
    groupby(['store_sys_id', 'store_name']). \
    agg({'customer_cnt': 'sum', 'order_cnt': 'sum', 'total_sales': 'sum',
         'total_t_points': 'sum'})
df_shopping_pmt_by_store['avg_customer_spent'] = df_shopping_pmt_by_store['total_sales'] / \
                                                 df_shopping_pmt_by_store['customer_cnt']
df_shopping_pmt_by_store['avg_order_value'] = df_shopping_pmt_by_store['total_sales'] / \
                                              df_shopping_pmt_by_store['order_cnt']
df_shopping_pmt_by_store['profit_margin'] = df_shopping_pmt_by_store['total_t_points'] / \
                                            df_shopping_pmt_by_store['total_sales']
df_shopping_pmt_by_store = df_shopping_pmt_by_store.sort_values(by='total_sales', ascending=False)
df_shopping_pmt_by_store = df_shopping_pmt_by_store.rename(columns={'customer_cnt': 'Customer Count #',
                                                                    'order_cnt': 'Order Count #',
                                                                    'total_sales': 'Total Sales $',
                                                                    'total_t_points': 'Total T-Points',
                                                                    'profit_margin': 'Profit Margin',
                                                                    'avg_customer_spent': 'Avg Customer Spent $',
                                                                    'avg_order_value': 'Avg Order Value $', })

# metrics dict
metrics_map = {
    'curr_fum': df_fum_curr[df_fum_curr.index == 'Total']['Active FUM'].values[0],
    'curr_reg_user': df_user_ss_d.shape[0],
    'curr_inv_user': df_inv_users[df_inv_users['total_bal'] > 0].shape[0],
}


##############
# Callbacks
##############
@app.callback(Output("page-content", "children"), [Input("url", "pathname")])
def display_page(pathname):
    if pathname == "/dash-financial-report/user_profile":
        return userProfile.create_layout(app, DATA_REFRESH_TS, metrics_map, df_user_ss_d, df_reg_users_counts,
                                         df_inv_users_counts, df_inv_users_counts_seed, df_inv_users_counts_spark)
    elif pathname == "/dash-financial-report/fund-performance":
        return fundPerformance.create_layout(app, DATA_REFRESH_TS, df_fum_curr, df_fum_si, df_fum_trend)
    # elif pathname == "/dash-financial-report/app-usage":
    #     return appUsage.create_layout(app, DATA_REFRESH_TS)
    elif pathname == "/dash-financial-report/shopping":
        return shopping.create_layout(app, DATA_REFRESH_TS, df_user_ss_d, df_merchant_store_ss_d,
                                      df_shopping_pmt_by_store)
    elif pathname == "/dash-financial-report/search":
        return search.create_layout(app, DATA_REFRESH_TS)
    elif pathname == "/dash-financial-report/full-view":
        return (
            overview.create_layout(app, DATA_REFRESH_TS, metrics_map, df_weekly_summary),
            userProfile.create_layout(app, DATA_REFRESH_TS, metrics_map, df_user_ss_d, df_reg_users_counts,
                                      df_inv_users_counts, df_inv_users_counts_seed, df_inv_users_counts_spark),
            fundPerformance.create_layout(app, DATA_REFRESH_TS, df_fum_curr, df_fum_si, df_fum_trend),
            # appUsage.create_layout(app, DATA_REFRESH_TS),
            shopping.create_layout(app, DATA_REFRESH_TS, df_user_ss_d, df_merchant_store_ss_d,
                                   df_shopping_pmt_by_store),
            search.create_layout(app, DATA_REFRESH_TS),
        )
    else:
        return overview.create_layout(app, DATA_REFRESH_TS, metrics_map, df_weekly_summary)


@callback(
    Output('tg-id-search-output-container', 'children'),
    [Input('btn-search-tg-id', 'n_clicks')],
    [
        State("input-tg-id", "value"),
    ],
)
def display_tg_id_search_result(search_tg_id, tg_id):
    if search_tg_id:
        if tg_id in df_user_ss_d['tanggram_id'].tolist():
            money = FormatTemplate.money(0)
            cols = ['tanggram_id', 'user_created_ts', 'state', 'gender', 'last_access_session_created_ts',
                    'refer_cnt', 'id_check_file_passed_ts', 'first_inv_created_ts', 'total_bal', 'total_deposit_cnt',
                    'total_deposit_amt', 'total_deposit_amt_in_prog', 'total_t_points_inv', 'total_inv_days',
                    'total_withdrawal_cnt', 'total_withdrawal_amt', 'total_withdrawal_amt_in_prog',
                    'total_interest_amt', 'seed_bal', 'spark_bal', 'groupbuy_order_amt']
            df = df_user_ss_d[df_user_ss_d['tanggram_id'] == int(tg_id)][cols]
            df['user_created_ts'] = df['user_created_ts'].dt.date
            df['last_access_session_created_ts'] = df['last_access_session_created_ts'].dt.date
            df['id_check_file_passed_ts'] = df['id_check_file_passed_ts'].dt.date
            df['first_inv_created_ts'] = df['first_inv_created_ts'].dt.date
            columns1 = [
                dict(id=cols[0], name='Tanggram ID', type='text'),
                dict(id=cols[1], name='User Created Date', type='text'),
                dict(id=cols[2], name='State', type='text'),
                dict(id=cols[3], name='Gender', type='text'),
                dict(id=cols[4], name='Last Access Session', type='text'),
                dict(id=cols[5], name='Referral', type='numeric'),
                dict(id=cols[6], name='ID Check Passed Date', type='text'),
                dict(id=cols[7], name='First Investment Date', type='text'),
            ]
            columns2 = [
                dict(id=cols[8], name='Total Balance $', type='numeric', format=money),
                dict(id=cols[9], name='Total Deposit #', type='numeric'),
                dict(id=cols[10], name='Total Deposit $', type='numeric', format=money),
                dict(id=cols[11], name='Total Deposit (in-progress) $', type='numeric', format=money),
                dict(id=cols[12], name='Total T-Points Invested', type='numeric'),
                dict(id=cols[13], name='Total Invested Days', type='numeric'),
                dict(id=cols[14], name='Total Withdrawal #', type='numeric'),
                dict(id=cols[15], name='Total Withdrawal $', type='numeric', format=money),
            ]
            columns3 = [
                dict(id=cols[16], name='Total Withdrawal (in-progress) $', type='numeric', format=money),
                dict(id=cols[17], name='Total Interest Distributed $', type='numeric', format=money),
                dict(id=cols[18], name='Seed Balance $', type='numeric', format=money),
                dict(id=cols[19], name='Spark Balance $', type='numeric', format=money),
                dict(id=cols[20], name='Group-buy Spent $', type='numeric', format=money),
            ]
            return DataTable(data=df.iloc[:, :len(columns1)].to_dict('records'),
                             columns=columns1,
                             # style_table={'overflowX': 'auto'},
                             style_cell={
                                 'height': 'auto',
                                 'width': 'auto', 'minWidth': '70px',
                                 'whiteSpace': 'normal',
                                 'fontSize': 12,
                                 'font-family': 'sans-serif'
                             }), \
                   DataTable(data=df.iloc[:, len(columns1):len(columns1)+len(columns2)].to_dict('records'),
                             columns=columns2,
                             style_cell={
                                 'height': 'auto',
                                 'width': 'auto', 'minWidth': '70px',
                                 'whiteSpace': 'normal',
                                 'fontSize': 12,
                                 'font-family': 'sans-serif'
                             }), \
                   DataTable(data=df.iloc[:, len(columns2):].to_dict('records'),
                             columns=columns3,
                             style_cell={
                                 'height': 'auto',
                                 'width': 'auto', 'minWidth': '70px',
                                 'whiteSpace': 'normal',
                                 'fontSize': 12,
                                 'font-family': 'sans-serif'
                             })
        else:
            return html.P("The Tanggram ID submitted is either invalid or does not exist.",
                          style={'color': 'red', 'fontSize': 12}),
    raise dash.exceptions.PreventUpdate


@callback(
    Output('tran-id-search-output-container', 'children'),
    [Input('btn-search-tran-id', 'n_clicks')],
    [
        State("input-tran-id", "value"),
    ],
)
def display_tran_id_search_result(search_tran_id, tran_id):
    if search_tran_id:
        if tran_id in df_inv_tran_ss_d['inv_tran_id'].tolist():
            money = FormatTemplate.money(0)
            cols = ['acc_nbr', 'curr_acc_bal', 'inv_id', 'fund_name', 'inv_state_name', 'inv_start_dt', 'inv_updated_at',
                    'inv_close_dt', 'inv_days', 'inv_tran_id', 'inv_tran_type_name', 'inv_tran_state_name',
                    'inv_tran_created_at', 'inv_tran_updated_at', 'inv_tran_amt']
            df = df_inv_tran_ss_d[df_inv_tran_ss_d['inv_tran_id'] == int(tran_id)][cols]
            df['inv_start_dt'] = df['inv_start_dt'].dt.date
            df['inv_updated_at'] = df['inv_updated_at'].dt.date
            df['inv_close_dt'] = df['inv_close_dt'].dt.date
            df['inv_tran_created_at'] = df['inv_tran_created_at'].dt.date
            df['inv_tran_updated_at'] = df['inv_tran_updated_at'].dt.date
            columns1 = [
                dict(id=cols[0], name='Account Number', type='text'),
                dict(id=cols[1], name='Current Account Balance', type='numeric', format=money),
                dict(id=cols[2], name='Investment ID', type='text'),
                dict(id=cols[3], name='Fund Name', type='text'),
                dict(id=cols[4], name='Investment State', type='text'),
                dict(id=cols[5], name='Investment Started Date', type='text'),
                dict(id=cols[6], name='Investment Updated Date', type='text'),
                dict(id=cols[7], name='Investment Closed Date', type='text'),
                dict(id=cols[8], name='Invested Days', type='numeric'),
            ]
            columns2 = [
                dict(id=cols[9], name='Transaction ID', type='text'),
                dict(id=cols[10], name='Transaction Type', type='text'),
                dict(id=cols[11], name='Transaction State', type='text'),
                dict(id=cols[12], name='Transaction Created Date', type='text'),
                dict(id=cols[13], name='Transaction Updated Date', type='text'),
                dict(id=cols[14], name='Transaction Amount', type='numeric', format=money),
            ]
            return DataTable(data=df.iloc[:, :len(columns1)].to_dict('records'),
                             columns=columns1,
                             style_cell={
                                 'height': 'auto',
                                 'width': 'auto', 'minWidth': '70px',
                                 'whiteSpace': 'normal',
                                 'fontSize': 12,
                                 'font-family': 'sans-serif'
                             }),\
                   DataTable(data=df.iloc[:, len(columns1):].to_dict('records'),
                             columns=columns2,
                             style_cell={
                                  'height': 'auto',
                                  'width': 'auto', 'minWidth': '70px',
                                  'whiteSpace': 'normal',
                                  'fontSize': 12,
                                  'font-family': 'sans-serif'
                              })
        else:
            return html.P("The transaction ID submitted is either invalid or does not exist.",
                          style={'color': 'red', 'fontSize': 12}),
    raise dash.exceptions.PreventUpdate


@app.callback(
    Output("pie-user-gender", "figure"),
    [Input("dd-user-metric", "value")])
def generate_gender_pie_chart(metric):
    # fig = px.pie(df_inv_user_genders.reset_index(), values=metric, names='gender', title='By Gender')
    fig = px.bar(df_inv_user_genders.reset_index(), x='gender', y=metric, color='gender', title='By Gender')
    fig.update_layout(showlegend=False)
    return fig


@app.callback(
    Output("pie-user-loc", "figure"),
    [Input("dd-user-metric", "value")])
def generate_location_pie_chart(metric):
    # fig = px.pie(df_inv_user_loc.reset_index(), values=metric, names='state', title='By Location')
    fig = px.bar(df_inv_user_loc.reset_index(), x='state', y=metric, color='state', title='By Location')
    fig.update_layout(showlegend=False)
    return fig


@app.callback(
    Output("bar-user-bal", "figure"),
    [Input("dd-user-metric", "value")])
def generate_bal_bar_chart(metric):
    fig = px.bar(df_inv_user_bal.reset_index(), x='balance_bins', y=metric, color='balance_bins',
                 title='By Account Balance')
    fig.update_layout(showlegend=False)
    return fig


@app.callback(
    Output("bar-user-inv-freq", "figure"),
    [Input("dd-user-metric", "value")])
def generate_inv_freq_bar_chart(metric):
    fig = px.bar(df_inv_user_inv_freq.reset_index(), x='deposit_bins', y=metric, color='deposit_bins',
                 title='By Deposit Counts')
    fig.update_layout(showlegend=False)
    return fig


if __name__ == "__main__":
    # start dash-app
    # app.run_server(debug=True, use_reloader=False)
    # app.run_server(host='localhost', port=8005, debug=False)
    app.run_server(debug=False, use_reloader=False)
