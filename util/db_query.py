import pandas as pd
from jinja2 import Template


def query_user_ss_d(db_engine):
    """
    Query User Daily Snapshot (all users)
    """
    query = Template(
        """
        SELECT * FROM public.mv_user_ss_d;
        """
    ).render(multi=True)

    with db_engine.connect() as connection:
        df = pd.read_sql_query(query, connection)

    return df


def query_inv_tran_ss_d(db_engine):
    """
    Query Investment Transaction Daily Snapshot (all transactions exc. cancelled)
    """
    query = Template(
        """
        select
            i.user_id
            ,i.acc_nbr
            ,i.curr_acc_bal
            ,i.inv_id
            ,i.fund_name
            ,"is"."name" AS inv_state_name
            ,i.inv_created_at
            ,i.inv_updated_at
            ,i.inv_start_dt
            ,i.inv_close_dt
            ,i.inv_days
            ,i.unit_number
            ,i.inv_total_bal
            ,i.inv_dist_opt
            ,it.inv_tran_id
            ,itt."name" AS inv_tran_type_name
            ,its."name" AS inv_tran_state_name
            ,it.inv_tran_created_at
            ,it.inv_tran_updated_at
            ,it.inv_tran_amt
        from 
            public.mv_inv_ss_d i
        inner join
            public.mv_rev_inv_tran_ss_d it
            on i.inv_id = it.inv_id
        inner join
            public.dim_inv_state "is"
            on i.inv_state = "is".id
        inner join
            public.dim_inv_tran_type itt
            on it.inv_tran_type = itt.id
        inner join
            public.dim_inv_tran_state its
            on it.inv_tran_state = its.id
        where
            its."name" != 'Cancelled'
        ;
        """
    ).render(multi=True)

    with db_engine.connect() as connection:
        df = pd.read_sql_query(query, connection)

    return df


def query_merchant_store_ss_d(db_engine):
    """
    Query Merchant & Store Daily Snapshot
    """
    query = Template(
        """
        SELECT * FROM public.mv_merchant_store_ss_d;
        """
    ).render(multi=True)

    with db_engine.connect() as connection:
        df = pd.read_sql_query(query, connection)

    return df


def query_shop_tran_ss_d(db_engine):
    """
    Query Shopping Transactions Daily Snapshot
    """
    query = Template(
        """
        select * from public.mv_shop_tran_ss_d;
        """
    ).render(multi=True)

    with db_engine.connect() as connection:
        df = pd.read_sql_query(query, connection)

    return df
