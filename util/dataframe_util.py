from datetime import datetime
import numpy as np
import pandas as pd
import pathlib
from util.db_query import query_user_ss_d, query_inv_tran_ss_d, query_merchant_store_ss_d, query_shop_tran_ss_d

# get relative data folder
PATH = pathlib.Path(__file__).parent
DATA_PATH = PATH.joinpath("../data").resolve()


def reload_all_data(db_engine, cutoff_dt_str):
    """
    Refresh all data queries
    Args:
        db_engine:

    Returns:

    """
    print(f'Refreshing data at {datetime.now()}...')
    # load user daily snapshot
    df_user_ss_d = query_user_ss_d(db_engine)
    df_user_ss_d = df_user_ss_d[df_user_ss_d['user_created_ts'] <= cutoff_dt_str]
    num_cols = df_user_ss_d.select_dtypes(include=['number']).columns
    df_user_ss_d[num_cols] = df_user_ss_d[num_cols].fillna(0)
    assert df_user_ss_d[df_user_ss_d['user_id'].duplicated()].shape[0] == 0
    print(f'df_user_ss_d: {df_user_ss_d.shape}')

    # load transaction data
    df_inv_tran_ss_d = query_inv_tran_ss_d(db_engine)
    df_inv_tran_ss_d = df_inv_tran_ss_d[df_inv_tran_ss_d['inv_tran_created_at'] <= cutoff_dt_str]
    print(
        f'df_inv_tran_ss_d: {df_inv_tran_ss_d.shape}; last investment transaction: {df_inv_tran_ss_d.inv_tran_updated_at.max()}')

    # load merchant daily snapshot
    df_merchant_store_ss_d = query_merchant_store_ss_d(db_engine)
    print(f'df_merchant_store_ss_d: {df_merchant_store_ss_d.shape}')

    # load shopping transactions
    df_shop_tran_ss_d = query_shop_tran_ss_d(db_engine)
    df_shop_tran_ss_d = df_shop_tran_ss_d[df_shop_tran_ss_d['order_updated_ts'] <= cutoff_dt_str]
    print(f'df_shop_tran_ss_d: {df_shop_tran_ss_d.shape}')

    # save to files
    df_user_ss_d.to_csv(DATA_PATH.joinpath("df_user_ss_d.csv"), index=False)
    df_inv_tran_ss_d.to_csv(DATA_PATH.joinpath("df_inv_tran_ss_d.csv"), index=False)
    df_merchant_store_ss_d.to_csv(DATA_PATH.joinpath("df_merchant_store_ss_d.csv"), index=False)
    df_shop_tran_ss_d.to_csv(DATA_PATH.joinpath("df_shop_tran_ss_d.csv"), index=False)

    return df_user_ss_d, df_inv_tran_ss_d, df_merchant_store_ss_d, df_shop_tran_ss_d


def count_new_users(df_user, freq='W'):
    df_reg_users = df_user.set_index('user_created_ts')[['user_id']]
    counts = df_reg_users.resample(freq)['user_id'].count()
    counts = pd.DataFrame(counts).rename(columns={'user_id': 'user_counts'})
    counts['diff'] = counts['user_counts'].diff()
    counts['diff_ratio'] = counts['diff'] / counts['user_counts'].shift(1)
    counts['diff_ratio'] = counts['diff_ratio'].fillna(0)
    counts['diff_ratio'] = counts['diff_ratio'].replace([np.inf, -np.inf], 1)
    counts = counts[counts.index >= '2019-07-01']

    return counts


def count_new_inv_users(df_user, freq='W'):
    # investment users
    df_inv_users = df_user[df_user['first_inv_created_ts'].notnull()]
    # count
    inv_user_counts = df_inv_users.set_index('first_inv_created_ts').resample(freq)['user_id'].count()
    # inv_user_counts = inv_user_counts[inv_user_counts.index <= cutoff_dt_str]  # cut off by last Sunday
    inv_user_counts = pd.DataFrame(inv_user_counts).rename(columns={'user_id': 'user_counts'})
    inv_user_counts['diff'] = inv_user_counts['user_counts'].diff()
    inv_user_counts['diff_ratio'] = inv_user_counts['diff'] / inv_user_counts['user_counts'].shift(1)
    inv_user_counts['diff_ratio'] = inv_user_counts['diff_ratio'].fillna(0)
    inv_user_counts['diff_ratio'] = inv_user_counts['diff_ratio'].replace([np.inf, -np.inf], 1)
    # seed inv users
    df_inv_users_seed = df_inv_users[df_inv_users['seed_first_inv_created_ts'].notnull()]
    inv_user_seed_counts = df_inv_users_seed.set_index('seed_first_inv_created_ts').resample(freq)['user_id'].count()
    inv_user_seed_counts = pd.DataFrame(inv_user_seed_counts).rename(columns={'user_id': 'user_counts'})
    inv_user_seed_counts['diff'] = inv_user_seed_counts['user_counts'].diff()
    inv_user_seed_counts['diff_ratio'] = inv_user_seed_counts['diff'] / inv_user_seed_counts['user_counts'].shift(1)
    inv_user_seed_counts['diff_ratio'] = inv_user_seed_counts['diff_ratio'].fillna(0)
    inv_user_seed_counts['diff_ratio'] = inv_user_seed_counts['diff_ratio'].replace([np.inf, -np.inf], 1)
    inv_user_seed_counts = inv_user_counts.reset_index()[['first_inv_created_ts']].merge(
        inv_user_seed_counts.reset_index(),
        left_on='first_inv_created_ts',
        right_on='seed_first_inv_created_ts',
        how='left'). \
        drop('seed_first_inv_created_ts', axis=1). \
        set_index('first_inv_created_ts').fillna(0)
    # spark inv users
    df_inv_users_spark = df_inv_users[df_inv_users['spark_first_inv_created_ts'].notnull()]
    inv_user_spark_counts = df_inv_users_spark.set_index('spark_first_inv_created_ts').resample(freq)['user_id'].count()
    inv_user_spark_counts = pd.DataFrame(inv_user_spark_counts).rename(columns={'user_id': 'user_counts'})
    inv_user_spark_counts['diff'] = inv_user_spark_counts['user_counts'].diff()
    inv_user_spark_counts['diff_ratio'] = inv_user_spark_counts['diff'] / inv_user_spark_counts['user_counts'].shift(1)
    inv_user_spark_counts['diff_ratio'] = inv_user_spark_counts['diff_ratio'].fillna(0)
    inv_user_spark_counts['diff_ratio'] = inv_user_spark_counts['diff_ratio'].replace([np.inf, -np.inf], 1)
    inv_user_spark_counts = inv_user_counts.reset_index()[['first_inv_created_ts']].merge(
        inv_user_spark_counts.reset_index(),
        left_on='first_inv_created_ts',
        right_on='spark_first_inv_created_ts',
        how='left'). \
        drop('spark_first_inv_created_ts', axis=1). \
        set_index('first_inv_created_ts').fillna(0)

    return inv_user_counts, inv_user_seed_counts, inv_user_spark_counts


def update_investments(df_user, df_inv_tran):
    # get unique investment accounts
    inv_cols = ['user_id', 'acc_nbr', 'inv_id', 'fund_name', 'inv_state_name', 'inv_created_at', 'inv_updated_at',
                'inv_start_dt', 'inv_close_dt', 'inv_days', 'inv_total_bal', 'inv_dist_opt']
    df_inv = df_inv_tran[inv_cols].drop_duplicates()
    assert round(df_user['total_bal'].sum()) == round(
        df_inv[~df_inv['inv_state_name'].isin(['Application In Progress'])]['inv_total_bal'].sum())
    # full withdrawal in progress
    df_withdrawal_in_prog = df_inv[(df_inv['inv_state_name'].isin(['Withdrawal', 'Withdrawal In Progress']))]
    # partial withdrawal in progress
    df_partial_withdrawal_in_prog = df_inv_tran[(df_inv_tran['inv_state_name'].isin(['Partial Withdrawal',
                                                                                     'Partial Withdrawal In Progress'])) &
                                                (df_inv_tran['inv_tran_type_name'].isin(['Partial Withdrawal'])) &
                                                (df_inv_tran['inv_tran_state_name'].isin(['Pending', 'In Progress']))]
    # deposit in-progress
    df_deposit_in_prog = df_inv_tran[(df_inv_tran['inv_state_name'].isin(['Application In Progress',
                                                                          'Update In Progress',
                                                                          'Pending Registration',
                                                                          'Pending After Error'])) &
                                     (df_inv_tran['inv_tran_type_name'].isin(['Deposit'])) &
                                     (df_inv_tran['inv_tran_state_name'].isin(['In Progress', 'Pending']))]

    return df_inv, df_withdrawal_in_prog, df_partial_withdrawal_in_prog, df_deposit_in_prog


def calc_fum_current(df_user_ss_d, df_withdrawal_in_prog, df_partial_withdrawal_in_prog, df_deposit_in_prog):
    total_fum = df_user_ss_d['total_bal'].sum()
    total_fum_in_withdrawal = df_withdrawal_in_prog['inv_total_bal'].sum() + df_partial_withdrawal_in_prog[
        'inv_tran_amt'].sum()
    total_fum_in_deposit = df_deposit_in_prog['inv_tran_amt'].sum()
    total_fum_net = total_fum - total_fum_in_withdrawal + total_fum_in_deposit

    seed_fum = df_user_ss_d['seed_bal'].sum()
    seed_fum_in_withdrawal = df_withdrawal_in_prog[df_withdrawal_in_prog['fund_name'] == 'Tanggram Seed'][
                                 'inv_total_bal'].sum() + \
                             df_partial_withdrawal_in_prog[
                                 df_partial_withdrawal_in_prog['fund_name'] == 'Tanggram Seed']['inv_tran_amt'].sum()
    seed_fum_in_deposit = df_deposit_in_prog[df_deposit_in_prog['fund_name'] == 'Tanggram Seed']['inv_tran_amt'].sum()
    seed_fum_net = seed_fum - seed_fum_in_withdrawal + seed_fum_in_deposit

    spark_fum = df_user_ss_d['spark_bal'].sum()
    spark_fum_in_withdrawal = df_withdrawal_in_prog[df_withdrawal_in_prog['fund_name'] == 'Tanggram Spark'][
                                  'inv_total_bal'].sum() + \
                              df_partial_withdrawal_in_prog[df_partial_withdrawal_in_prog['fund_name'] ==
                                                            'Tanggram Spark']['inv_tran_amt'].sum()
    spark_fum_in_deposit = df_deposit_in_prog[df_deposit_in_prog['fund_name'] == 'Tanggram Spark']['inv_tran_amt'].sum()
    spark_fum_net = spark_fum - spark_fum_in_withdrawal + spark_fum_in_deposit

    # create a dataframe
    df = pd.DataFrame([[total_fum, seed_fum, spark_fum],
                       [-total_fum_in_withdrawal, -seed_fum_in_withdrawal, -spark_fum_in_withdrawal],
                       [total_fum_in_deposit, seed_fum_in_deposit, spark_fum_in_deposit],
                       [total_fum_net, seed_fum_net, spark_fum_net],
                       [df_user_ss_d[(df_user_ss_d['total_bal'] > 0)].shape[0],
                        df_user_ss_d[df_user_ss_d['seed_bal'] > 0].shape[0],
                        df_user_ss_d[df_user_ss_d['spark_bal'] > 0].shape[0]],
                       [df_user_ss_d[(df_user_ss_d['total_bal'] > 0)]['total_deposit_amt'].sum() /
                        df_user_ss_d[(df_user_ss_d['total_bal'] > 0)].shape[0],
                        df_user_ss_d[(df_user_ss_d['seed_bal'] > 0)]['seed_deposit_amt'].sum() /
                        df_user_ss_d[(df_user_ss_d['seed_bal'] > 0)].shape[0],
                        df_user_ss_d[(df_user_ss_d['spark_bal'] > 0)]['spark_deposit_amt'].sum() /
                        df_user_ss_d[(df_user_ss_d['spark_bal'] > 0)].shape[0]],
                       [df_user_ss_d[(df_user_ss_d['total_bal'] > 0)]['total_deposit_amt'].median(),
                        df_user_ss_d[(df_user_ss_d['seed_bal'] > 0)]['seed_deposit_amt'].median(),
                        df_user_ss_d[(df_user_ss_d['spark_bal'] > 0)]['spark_deposit_amt'].median()]
                       ]).T
    df.columns = ['Active FUM', 'Withdrawal in-progress', 'Deposit in-progress', 'Net FUM', 'Active investors',
                  'Avg. deposit $', 'Median deposit $']
    df.index = ['Total', 'Seed', 'Spark']

    return df


def calc_fum_since_inception(df_user):
    df = pd.DataFrame([[df_user[df_user['total_deposit_amt'] > 0].shape[0],
                        df_user[df_user['seed_deposit_amt'] > 0].shape[0],
                        df_user[df_user['spark_deposit_amt'] > 0].shape[0]],
                       [df_user['total_deposit_amt'].sum(), df_user['seed_deposit_amt'].sum(),
                        df_user['spark_deposit_amt'].sum()],
                       [-df_user['total_withdrawal_amt'].sum(), -df_user['seed_withdrawal_amt'].sum(),
                        -df_user['spark_withdrawal_amt'].sum()],
                       [df_user['total_interest_amt'].sum(), df_user['seed_interest_amt'].sum(),
                        df_user['spark_interest_amt'].sum()],
                       [df_user['total_t_points_inv'].sum(), df_user['seed_t_points_inv'].sum(),
                        df_user['spark_t_points_inv'].sum()],
                       [df_user['total_deposit_amt'].sum() /
                        df_user[(df_user['total_deposit_amt'] > 0)].shape[0],
                        df_user['seed_deposit_amt'].sum() /
                        df_user[(df_user['seed_deposit_amt'] > 0)].shape[0],
                        df_user['spark_deposit_amt'].sum() /
                        df_user[(df_user['spark_deposit_amt'] > 0)].shape[0]],
                       [df_user[df_user['total_deposit_amt'] > 0]['total_deposit_amt'].median(),
                        df_user[df_user['seed_deposit_amt'] > 0]['seed_deposit_amt'].median(),
                        df_user[df_user['spark_deposit_amt'] > 0]['spark_deposit_amt'].median()],
                       [df_user['total_fees'].sum(), df_user['seed_fees'].sum(),
                        df_user['spark_fees'].sum()]
                       ]).T
    df.columns = ['Unique investors #', 'Total deposit $', 'Total withdrawal $', 'Total interest $',
                  'Total T-Points invested', 'Avg deposit $', 'Median deposit $', 'Fees $']
    df.index = ['Total', 'Seed', 'Spark']

    return df


def update_investment_transactions(df_inv_tran_ss_d):
    # all complete and in-progress transactions
    df_inv_trans = df_inv_tran_ss_d[
        df_inv_tran_ss_d['inv_tran_state_name'].isin(['Complete', 'In Progress', 'Pending'])].set_index(
        'inv_tran_created_at')
    df_inv_trans.loc[df_inv_trans['inv_tran_type_name'] == 'Partial Withdrawal', 'inv_tran_type_name'] = 'Withdrawal'
    df_inv_trans_grp = df_inv_trans.groupby(['fund_name', 'inv_tran_type_name']).resample("W")[
        'inv_tran_amt'].sum().unstack('fund_name', fill_value=0).reset_index()
    df_inv_trans_grp = df_inv_trans_grp[
        (df_inv_trans_grp['inv_tran_type_name'].isin(['Deposit', 'Withdrawal', 'Interest', 'From Reward']))]

    # interests re-invested
    df_inv_trans_grp2 = df_inv_tran_ss_d[
        (df_inv_tran_ss_d['inv_tran_state_name'].isin(['Complete', 'In Progress', 'Pending'])) &
        (df_inv_tran_ss_d['inv_dist_opt'] == 1)].set_index('inv_tran_created_at')
    df_inv_trans_grp2 = df_inv_trans_grp2.groupby(['fund_name', 'inv_tran_type_name']).resample("W")[
        'inv_tran_amt'].sum().unstack('fund_name', fill_value=0).reset_index()
    df_inv_trans_grp2 = df_inv_trans_grp2[(df_inv_trans_grp2['inv_tran_type_name'].isin(['Interest']))]
    df_inv_trans_grp2.loc[
        df_inv_trans_grp2['inv_tran_type_name'] == 'Interest', 'inv_tran_type_name'] = 'Interest Reinvested'

    # combine
    df_inv_trans_grp = df_inv_trans_grp.append(df_inv_trans_grp2)
    df_inv_trans_grp.loc[:, 'Total'] = df_inv_trans_grp['Tanggram Seed'] + df_inv_trans_grp['Tanggram Spark']
    df_inv_trans_grp = df_inv_trans_grp.sort_values(by=['inv_tran_type_name', 'inv_tran_created_at'])

    # cumulative sum
    df_inv_trans_grp_cum = df_inv_trans_grp.groupby(['inv_tran_type_name', 'inv_tran_created_at']).sum().groupby(
        level=0).cumsum().reset_index()


def weekly_change_summary(df_user, df_inv_tran):
    # new user counts
    df_reg_users_cnts = count_new_users(df_user, freq='W')
    # investment user counts
    df_inv_users_cnts, df_inv_users_cnts_seed, df_inv_users_cnts_spark = count_new_inv_users(df_user,
                                                                                             freq='W')

    # by transaction created date
    df_inv_trans_created = df_inv_tran[
        df_inv_tran['inv_tran_state_name'].isin(['Complete', 'In Progress', 'Pending'])].set_index(
        'inv_tran_created_at')
    df_inv_trans_created.loc[
        df_inv_trans_created['inv_tran_type_name'] == 'Partial Withdrawal', 'inv_tran_type_name'] = 'Withdrawal'
    df_inv_trans_created_grp = \
        df_inv_trans_created[df_inv_trans_created['inv_tran_type_name'].isin(['Deposit', 'Withdrawal'])]. \
            groupby(['fund_name', 'inv_tran_type_name']). \
            resample("W")['inv_tran_amt'].sum(). \
            unstack('fund_name', fill_value=0).reset_index().set_index('inv_tran_created_at')
    df_inv_trans_created_grp2 = \
        df_inv_trans_created[df_inv_trans_created['inv_tran_type_name'].isin(['Deposit', 'Withdrawal'])]. \
            groupby(['fund_name', 'inv_tran_type_name']). \
            resample("W")['user_id'].nunique(). \
            unstack('fund_name', fill_value=0).reset_index().set_index('inv_tran_created_at')

    # by transaction updated date
    df_inv_trans_comp = df_inv_tran[df_inv_tran['inv_tran_state_name'].isin(['Complete'])].set_index(
        'inv_tran_updated_at')
    df_inv_trans_comp.loc[
        df_inv_trans_comp['inv_tran_type_name'] == 'Partial Withdrawal', 'inv_tran_type_name'] = 'Withdrawal'
    df_inv_trans_comp_grp = df_inv_trans_comp[df_inv_trans_comp['inv_tran_type_name'].isin(['Deposit', 'Withdrawal'])]. \
        groupby(['fund_name', 'inv_tran_type_name']). \
        resample("W")['inv_tran_amt'].sum(). \
        unstack('fund_name', fill_value=0).reset_index().set_index('inv_tran_updated_at')
    df_inv_trans_comp_grp2 = df_inv_trans_comp[df_inv_trans_comp['inv_tran_type_name'].isin(['Deposit', 'Withdrawal'])]. \
        groupby(['fund_name', 'inv_tran_type_name']). \
        resample("W")['user_id'].nunique(). \
        unstack('fund_name', fill_value=0).reset_index().set_index('inv_tran_updated_at')

    # start with weekly new users
    df_weekly_summary = df_reg_users_cnts.tail(5)[['user_counts']].rename(
        columns={'user_counts': 'New Registered Users'})
    # add weekly new seed users
    df_weekly_summary = df_weekly_summary.merge(
        df_inv_users_cnts_seed[['user_counts']].rename(columns={'user_counts': 'Seed - New Investors'}),
        left_index=True, right_index=True, how='left')
    df_weekly_summary = df_weekly_summary.merge(
        df_inv_trans_comp_grp[df_inv_trans_comp_grp['inv_tran_type_name'] == 'Deposit'][['Tanggram Seed']]. \
            rename(columns={'Tanggram Seed': 'Seed - Deposit (completed) $'}),
        left_index=True, right_index=True, how='left')
    df_weekly_summary = df_weekly_summary.merge(
        df_inv_trans_created_grp[df_inv_trans_created_grp['inv_tran_type_name'] == 'Deposit'][['Tanggram Seed']]. \
            rename(columns={'Tanggram Seed': 'Seed - Deposit (created) $'}),
        left_index=True, right_index=True, how='left')
    df_weekly_summary = df_weekly_summary.merge(
        df_inv_trans_created_grp2[df_inv_trans_created_grp2['inv_tran_type_name'] == 'Deposit'][['Tanggram Seed']]. \
            rename(columns={'Tanggram Seed': 'Seed - Deposit (created) #'}),
        left_index=True, right_index=True, how='left')
    df_weekly_summary = df_weekly_summary.merge(
        df_inv_trans_comp_grp[df_inv_trans_comp_grp['inv_tran_type_name'] == 'Withdrawal'][['Tanggram Seed']]. \
            rename(columns={'Tanggram Seed': 'Seed - Withdrawal (completed) $'}),
        left_index=True, right_index=True, how='left')
    df_weekly_summary['Seed - Withdrawal (completed) $'] = -df_weekly_summary['Seed - Withdrawal (completed) $']
    df_weekly_summary = df_weekly_summary.merge(
        df_inv_trans_created_grp[df_inv_trans_created_grp['inv_tran_type_name'] == 'Withdrawal'][['Tanggram Seed']]. \
            rename(columns={'Tanggram Seed': 'Seed - Withdrawal (created) $'}),
        left_index=True, right_index=True, how='left')
    df_weekly_summary['Seed - Withdrawal (created) $'] = -df_weekly_summary['Seed - Withdrawal (created) $']
    df_weekly_summary = df_weekly_summary.merge(
        df_inv_trans_created_grp2[df_inv_trans_created_grp2['inv_tran_type_name'] == 'Withdrawal'][['Tanggram Seed']]. \
            rename(columns={'Tanggram Seed': 'Seed - Withdrawal (created) #'}),
        left_index=True, right_index=True, how='left')
    # add weekly new spark users
    df_weekly_summary = df_weekly_summary.merge(
        df_inv_users_cnts_spark[['user_counts']].rename(columns={'user_counts': 'Spark - New Investors'}),
        left_index=True, right_index=True, how='left')
    df_weekly_summary = df_weekly_summary.merge(
        df_inv_trans_comp_grp[df_inv_trans_comp_grp['inv_tran_type_name'] == 'Deposit'][['Tanggram Spark']]. \
            rename(columns={'Tanggram Spark': 'Spark - Deposit (completed) $'}),
        left_index=True, right_index=True, how='left')
    df_weekly_summary = df_weekly_summary.merge(
        df_inv_trans_created_grp[df_inv_trans_created_grp['inv_tran_type_name'] == 'Deposit'][['Tanggram Spark']]. \
            rename(columns={'Tanggram Spark': 'Spark - Deposit (created) $'}),
        left_index=True, right_index=True, how='left')
    df_weekly_summary = df_weekly_summary.merge(
        df_inv_trans_created_grp2[df_inv_trans_created_grp2['inv_tran_type_name'] == 'Deposit'][['Tanggram Spark']]. \
            rename(columns={'Tanggram Spark': 'Spark - Deposit (created) #'}),
        left_index=True, right_index=True, how='left')
    df_weekly_summary = df_weekly_summary.merge(
        df_inv_trans_comp_grp[df_inv_trans_comp_grp['inv_tran_type_name'] == 'Withdrawal'][['Tanggram Spark']]. \
            rename(columns={'Tanggram Spark': 'Spark - Withdrawal (completed) $'}),
        left_index=True, right_index=True, how='left')
    df_weekly_summary['Spark - Withdrawal (completed) $'] = -df_weekly_summary['Spark - Withdrawal (completed) $']
    df_weekly_summary = df_weekly_summary.merge(
        df_inv_trans_created_grp[df_inv_trans_created_grp['inv_tran_type_name'] == 'Withdrawal'][['Tanggram Spark']]. \
            rename(columns={'Tanggram Spark': 'Spark - Withdrawal (created) $'}),
        left_index=True, right_index=True, how='left')
    df_weekly_summary['Spark - Withdrawal (created) $'] = -df_weekly_summary['Spark - Withdrawal (created) $']
    df_weekly_summary = df_weekly_summary.merge(
        df_inv_trans_created_grp2[df_inv_trans_created_grp2['inv_tran_type_name'] == 'Withdrawal'][['Tanggram Spark']]. \
            rename(columns={'Tanggram Spark': 'Spark - Withdrawal (created) #'}),
        left_index=True, right_index=True, how='left')
    # clean up
    df_weekly_summary = df_weekly_summary.fillna(0)
    df_weekly_summary.index = df_weekly_summary.index.rename('Week End')
    df_weekly_summary = df_weekly_summary.sort_index(ascending=False)

    return df_weekly_summary


def calc_fum_trends(df_user, df_inv_tran, freq='W'):
    """
    FUM trends

    Args:
        df_user:
        df_inv_tran:
        freq:

    Returns:

    """
    # new registered users
    df_new_user_freq = df_user.set_index('user_created_ts')[['user_id']].resample(freq).count().rename(
        columns={'user_id': 'new_app_users'})

    # transactions by freq
    df_deposit_freq = df_inv_tran[(df_inv_tran['inv_tran_type_name'] == 'Deposit') &
                                  (df_inv_tran['inv_tran_state_name'] == 'Complete')]. \
        set_index('inv_tran_updated_at').resample(freq).agg({'user_id': 'nunique', 'inv_tran_amt': 'sum'}). \
        rename(columns={'user_id': 'deposit_users', 'inv_tran_amt': 'deposit_amt'})

    df_tpoints_freq = df_inv_tran[(df_inv_tran['inv_tran_type_name'] == 'From Reward') &
                                  (df_inv_tran['inv_tran_state_name'] == 'Complete')]. \
        set_index('inv_tran_updated_at').resample(freq).agg({'inv_tran_amt': 'sum'}). \
        rename(columns={'inv_tran_amt': 't_points'})

    df_interest_reinv_freq = df_inv_tran[(df_inv_tran['inv_tran_type_name'] == 'Interest') &
                                         (df_inv_tran['inv_tran_state_name'] == 'Complete') &
                                         (df_inv_tran['inv_dist_opt'] == 1)]. \
        set_index('inv_tran_updated_at').resample(freq).agg({'user_id': 'nunique', 'inv_tran_amt': 'sum'}). \
        rename(columns={'user_id': 'interest_reinv_users', 'inv_tran_amt': 'interest_reinv'})

    df_interest_dist_freq = df_inv_tran[(df_inv_tran['inv_tran_type_name'] == 'Interest') &
                                        (df_inv_tran['inv_tran_state_name'] == 'Complete') &
                                        (df_inv_tran['inv_dist_opt'] == 2)]. \
        set_index('inv_tran_updated_at').resample(freq).agg({'user_id': 'nunique', 'inv_tran_amt': 'sum'}). \
        rename(columns={'user_id': 'interest_dist_users', 'inv_tran_amt': 'interest_dist'})

    df_wd_freq = df_inv_tran[(df_inv_tran['inv_tran_type_name'].isin(['Withdrawal', 'Partial Withdrawal'])) &
                             (df_inv_tran['inv_tran_state_name'] == 'Complete')]. \
        set_index('inv_tran_updated_at').resample(freq).agg({'user_id': 'nunique', 'inv_tran_amt': 'sum'}). \
        rename(columns={'user_id': 'withdrawal_users', 'inv_tran_amt': 'withdrawal_amt'})

    df_fee_freq = df_inv_tran[(df_inv_tran['inv_tran_type_name'] == 'Fee') &
                              (df_inv_tran['inv_tran_state_name'] == 'Complete')]. \
        set_index('inv_tran_updated_at').resample(freq).agg({'inv_tran_amt': 'sum'}). \
        rename(columns={'inv_tran_amt': 'fee'})

    df_tax_freq = df_inv_tran[(df_inv_tran['inv_tran_type_name'] == 'Withholding Tax') &
                              (df_inv_tran['inv_tran_state_name'] == 'Complete')]. \
        set_index('inv_tran_updated_at').resample(freq).agg({'inv_tran_amt': 'sum'}). \
        rename(columns={'inv_tran_amt': 'tax'})

    # merge
    df_tran_freq = df_new_user_freq.merge(df_deposit_freq, left_index=True, right_index=True, how='inner')
    df_tran_freq = df_tran_freq.merge(df_tpoints_freq, left_index=True, right_index=True, how='left')
    df_tran_freq = df_tran_freq.merge(df_interest_reinv_freq, left_index=True, right_index=True, how='left')
    df_tran_freq = df_tran_freq.merge(df_interest_dist_freq, left_index=True, right_index=True, how='left')
    df_tran_freq = df_tran_freq.merge(df_fee_freq, left_index=True, right_index=True, how='left')
    df_tran_freq = df_tran_freq.merge(df_tax_freq, left_index=True, right_index=True, how='left')
    df_tran_freq = df_tran_freq.merge(df_wd_freq, left_index=True, right_index=True, how='left')
    df_tran_freq = df_tran_freq.fillna(0)
    df_tran_freq['net_amt'] = df_tran_freq['deposit_amt'] + df_tran_freq['t_points'] + df_tran_freq['interest_reinv'] \
                              - df_tran_freq['fee'] - df_tran_freq['tax'] - df_tran_freq['withdrawal_amt']
    df_tran_freq['net_fum'] = df_tran_freq['net_amt'].cumsum()

    return df_tran_freq
