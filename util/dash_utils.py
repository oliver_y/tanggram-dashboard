from dash import html
from dash import dcc


def Header(app, ts):
    return html.Div([get_header(app, ts), html.Br([]), get_menu()])


def get_header(app, ts):
    header = html.Div(
        [
            html.Div(
                [
                    html.A(
                        html.Img(
                            src=app.get_asset_url("Tanggram-logo.png"),
                            className="logo",
                            style={'height': '20%', 'width': '20%'}
                        ),
                        href="https://www.tanggram.com/",
                    ),
                    # html.A(
                    #     html.Button(
                    #         "Enterprise Demo",
                    #         id="learn-more-button",
                    #         style={"margin-left": "-10px"},
                    #     ),
                    #     href="https://plotly.com/get-demo/",
                    # ),
                    # html.A(
                    #     html.Button("Source Code", id="learn-more-button"),
                    #     href="https://github.com/plotly/dash-sample-apps/tree/main/apps/dash-financial-report",
                    # ),
                ],
                className="row",
            ),
            html.Div(
                [
                    html.Div(
                        [html.H5("Tanggram Data Analytics - Interactive Dashboards (Draft v0.1)"),
                         html.H6(f"Data refreshed as at {ts}")],
                        className="seven columns main-title",
                    ),
                    html.Div(
                        [
                            dcc.Link(
                                "Full View",
                                href="/dash-financial-report/full-view",
                                className="full-view-link",
                            )
                        ],
                        className="five columns",
                    ),
                ],
                className="twelve columns",
                style={"padding-left": "0"},
            ),
        ],
        className="row",
    )
    return header


def get_menu():
    menu = html.Div(
        [
            dcc.Link(
                "Overview",
                href="/dash-financial-report/overview",
                className="tab first",
            ),
            dcc.Link(
                "User Profile",
                href="/dash-financial-report/user_profile",
                className="tab",
            ),
            dcc.Link(
                "Fund Performance",
                href="/dash-financial-report/fund-performance",
                className="tab",
            ),
            # dcc.Link(
            #     "App Usage",
            #     href="/dash-financial-report/app-usage",
            #     className="tab",
            # ),
            dcc.Link(
                "Shopping",
                href="/dash-financial-report/shopping",
                className="tab"
            ),
            dcc.Link(
                "Search",
                href="/dash-financial-report/search",
                className="tab",
            ),
        ],
        className="row all-tabs",
    )
    return menu


def make_dash_table(df, inc_index=False, index_header=''):
    """ Return a dash definition of an HTML table for a Pandas dataframe """
    table = []
    # append header row
    html_row = []
    if inc_index:
        html_row.append(index_header)
    for i in range(len(df.columns)):
        html_row.append(html.Td([df.columns[i]]))
    table.append(html.Tr(html_row))
    # append dataframe content
    for index, row in df.iterrows():
        html_row = []
        if inc_index:
            html_row.append(index)
        for i in range(len(row)):
            html_row.append(html.Td([row[i]]))
        table.append(html.Tr(html_row))
    return table
