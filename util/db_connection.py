import json
from sqlalchemy import create_engine


def connect_to_analytical_db():
    """
    # Set up DB connections
    Returns:

    """
    with open('config/db_connection.json') as f:
        connect_info = json.load(f)

    with open('credential/credential.json') as f:
        credential = json.load(f)

    analytical_db_uri = 'postgresql+psycopg2://{user}:{password}@{server}:{port}/{database}'.format(
        user=credential['analytical_db']['user'],
        password=credential['analytical_db']['password'],
        server=connect_info['analytical_db']['server_host'],
        port=connect_info['analytical_db']['port'],
        database=connect_info['analytical_db']['database'])

    return create_engine(analytical_db_uri)


if __name__ == '__main__':
    # test connection
    db_engine = connect_to_analytical_db()
    assert db_engine.name == 'postgresql'
