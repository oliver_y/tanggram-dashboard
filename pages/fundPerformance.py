from dash import dcc, html
import plotly.graph_objs as go
from dash.dash_table import DataTable, FormatTemplate
from util.dash_utils import Header


def create_layout(app, ts, df_fum_curr, df_fum_si, df_fum_trend):

    money = FormatTemplate.money(0)

    # current FUM
    df = df_fum_curr.reset_index()
    cols = df.columns
    columns1 = [
        dict(id=cols[0], name='', type='text'),
        dict(id=cols[1], name=cols[1], type='numeric', format=money),
        dict(id=cols[2], name=cols[2], type='numeric', format=money),
        dict(id=cols[3], name=cols[3], type='numeric', format=money),
        dict(id=cols[4], name=cols[4], type='numeric', format=money),
        dict(id=cols[5], name=cols[5], type='numeric'),
        dict(id=cols[6], name=cols[6], type='numeric', format=money),
        dict(id=cols[7], name=cols[7], type='numeric', format=money),
    ]
    data1 = df[cols].to_dict('records')

    # FUM since inception
    df = df_fum_si.reset_index()
    cols = df.columns
    columns2 = [
        dict(id=cols[0], name='', type='text'),
        dict(id=cols[1], name='Investors #', type='numeric'),
        dict(id=cols[2], name=cols[2], type='numeric', format=money),
        dict(id=cols[3], name=cols[3], type='numeric', format=money),
        dict(id=cols[4], name='Total distribution $', type='numeric', format=money),
        dict(id=cols[5], name=cols[5], type='numeric'),
        dict(id=cols[6], name=cols[6], type='numeric', format=money),
        dict(id=cols[7], name=cols[7], type='numeric', format=money),
        dict(id=cols[8], name='Membership Fee $', type='numeric', format=money),
    ]
    data2 = df[cols].to_dict('records')

    return html.Div(
        [
            Header(app, ts),
            # Page 3
            html.Div(
                [
                    # Row 1 - FUM table (current)
                    html.Div(
                        [
                            html.Div(
                                [
                                    html.H6(
                                        ["Fund Under Management (Current)"], className="subtitle padded"
                                    ),
                                    DataTable(
                                        data=data1,
                                        columns=columns1,
                                        style_cell={
                                            'height': 'auto',
                                            'width': 'auto', 'minWidth': '90px',
                                            'whiteSpace': 'normal',
                                            'fontSize': 12,
                                            'font-family': 'sans-serif'
                                        },
                                    ),
                                ],
                                className="six columns",
                            )
                        ],
                        className="row ",
                    ),
                    # Row 2 - FUM table (since inception)
                    html.Div(
                        [
                            html.Div(
                                [
                                    html.H6(
                                        ["Fund Under Management (Since Inception)"], className="subtitle padded"
                                    ),
                                    DataTable(
                                        data=data2,
                                        columns=columns2,
                                        style_cell={
                                            'height': 'auto',
                                            'width': 'auto', 'minWidth': '80px',
                                            'whiteSpace': 'normal',
                                            'fontSize': 12,
                                            'font-family': 'sans-serif'
                                        },
                                    ),
                                ],
                                className="six columns",
                            )
                        ],
                        className="row ",
                    ),
                    # Row 3 - FUM trend
                    html.Div(
                        [
                            html.Div(
                                [
                                    html.H6("FUM Trends", className="subtitle padded"),
                                    dcc.Graph(
                                        id="graph-4",
                                        figure={
                                            "data": [
                                                go.Bar(
                                                    x=df_fum_trend.index,
                                                    y=df_fum_trend["deposit_amt"],
                                                    # line={"color": "#97151c"},
                                                    # mode="lines",
                                                    name="Deposits (completed)",
                                                    yaxis='y1'
                                                ),
                                                go.Bar(
                                                    x=df_fum_trend.index,
                                                    y=df_fum_trend["withdrawal_amt"],
                                                    # line={"color": "#b5b5b5"},
                                                    # mode="lines",
                                                    name="Withdrawals (completed)",
                                                    yaxis='y1'
                                                ),
                                                go.Scatter(
                                                    x=df_fum_trend.index,
                                                    y=df_fum_trend["net_fum"],
                                                    line={"color": "#b5b5b5"},
                                                    mode="lines",
                                                    name="Net FUM",
                                                    yaxis='y2'
                                                ),
                                            ],
                                            "layout": go.Layout(
                                                autosize=True,
                                                width=700,
                                                height=200,
                                                font={"family": "Raleway", "size": 10},
                                                margin={
                                                    "r": 30,
                                                    "t": 30,
                                                    "b": 30,
                                                    "l": 30,
                                                },
                                                showlegend=True,
                                                titlefont={
                                                    "family": "Raleway",
                                                    "size": 10,
                                                },
                                                xaxis={
                                                    "autorange": True,
                                                    "range": [
                                                        "2007-12-31",
                                                        "2018-03-06",
                                                    ],
                                                    "rangeselector": {
                                                        "buttons": [
                                                            {
                                                                "count": 1,
                                                                "label": "1Y",
                                                                "step": "year",
                                                                "stepmode": "backward",
                                                            },
                                                            {
                                                                "count": 3,
                                                                "label": "3Y",
                                                                "step": "year",
                                                                "stepmode": "backward",
                                                            },
                                                            {
                                                                "count": 5,
                                                                "label": "5Y",
                                                                "step": "year",
                                                            },
                                                            {
                                                                "count": 10,
                                                                "label": "10Y",
                                                                "step": "year",
                                                                "stepmode": "backward",
                                                            },
                                                            {
                                                                "label": "All",
                                                                "step": "all",
                                                            },
                                                        ]
                                                    },
                                                    "showline": True,
                                                    "type": "date",
                                                    "zeroline": False,
                                                },
                                                yaxis={
                                                    "autorange": True,
                                                    # "range": [
                                                    #     18.6880162434,
                                                    #     278.431996757,
                                                    # ],
                                                    "showline": True,
                                                    "type": "linear",
                                                    "zeroline": False,
                                                },
                                                yaxis2={
                                                    "autorange": True,
                                                    "showline": True,
                                                    "type": "linear",
                                                    "zeroline": False,
                                                    "overlaying": 'y',
                                                    "side": 'right'
                                                },
                                            ),
                                        },
                                        config={"displayModeBar": False},
                                    ),
                                ],
                                className="twelve columns",
                            )
                        ],
                        className="row ",
                    ),
                ],
                className="sub_page",
            ),
        ],
        className="page",
    )
