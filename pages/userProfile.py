import plotly.graph_objs as go
from dash import dcc, html
from util.dash_utils import Header


def create_layout(app, ts, metrics, df_user_ss_d, df_reg_users_cnts, df_inv_users_cnts, df_inv_users_cnts_seed,
                  df_inv_users_cnts_spark):
    return html.Div(
        [
            Header(app, ts),
            # Page 2
            html.Div(
                [
                    # Totals
                    html.Div(
                        [
                            html.Div(
                                [
                                    html.H6(
                                        ["Total Registered Users"]
                                    ),
                                    html.H6(
                                        [f'{metrics["curr_reg_user"]:,.0f}']
                                    ),
                                ],
                                className="three columns",
                            ),
                            html.Div(
                                [
                                    html.H6(
                                        ["Current Investment Users"]
                                    ),
                                    html.H6(
                                        [f'{metrics["curr_inv_user"]:,.0f}']
                                    ),
                                ],
                                className="three columns",
                            ),
                            html.Div(
                                [
                                    html.H6(
                                        ["Avg. Age of Investment Users"]
                                    ),
                                    html.H6(
                                        [str(round(df_user_ss_d[df_user_ss_d['age'] > 0]['age'].mean(), 1))]
                                    ),
                                ],
                                className="three columns",
                            ),
                            html.Div(
                                [
                                    html.H6(
                                        ["Median Age of Investment Users"]
                                    ),
                                    html.H6(
                                        [str(round(df_user_ss_d[df_user_ss_d['age'] > 0]['age'].median()))]
                                    ),
                                ],
                                className="three columns",
                            ),
                        ],
                        className="row",
                        style={"margin-bottom": "35px"},
                    ),
                    # user trends charts
                    html.Div([
                        html.H6("Weekly New Registered Users", className="subtitle padded"),
                        ],
                        className="row",
                        style={"margin-bottom": "35px"},
                    ),
                    html.Div(children=[
                        dcc.Graph(
                            id="graph-1",
                            figure={
                                "data": [
                                    go.Scatter(
                                        x=df_reg_users_cnts.index.tolist(),
                                        y=df_reg_users_cnts['user_counts'].tolist(),
                                        # line={"color": "#97151c"},
                                        # mode="lines",
                                    )
                                ],
                            },
                            # style={'display': 'inline-block'}
                            # config={"displayModeBar": False},
                        ),
                        dcc.Graph(
                            id="graph-2",
                            figure={
                                "data": [
                                    go.Scatter(
                                        x=df_reg_users_cnts['user_counts'].cumsum().index,
                                        y=df_reg_users_cnts['user_counts'].cumsum().values,
                                    )
                                ],
                            },
                            # style={'display': 'inline-block'}
                            # config={"displayModeBar": False},
                        ),
                    ]),
                    # Investment user trends charts
                    html.Div([
                        html.H6("Weekly New Investment Users", className="subtitle padded"),
                    ],
                        className="row",
                        style={"margin-bottom": "35px"},
                    ),
                    html.Div(children=[
                        dcc.Graph(
                            id="graph-3",
                            figure={
                                "data": [
                                    go.Scatter(
                                        x=df_inv_users_cnts.index.tolist(),
                                        y=df_inv_users_cnts['user_counts'].tolist(),
                                        name='All investors'
                                    ),
                                    go.Scatter(
                                        x=df_inv_users_cnts_seed.index.tolist(),
                                        y=df_inv_users_cnts_seed['user_counts'].tolist(),
                                        name='Seed investors'
                                    ),
                                    go.Scatter(
                                        x=df_inv_users_cnts_spark.index.tolist(),
                                        y=df_inv_users_cnts_spark['user_counts'].tolist(),
                                        name='Spark investors'
                                    )
                                ],
                            },
                            config={"displayModeBar": False},
                        ),
                        dcc.Graph(
                            id="graph-4",
                            figure={
                                "data": [
                                    go.Scatter(
                                        x=df_inv_users_cnts['user_counts'].cumsum().index,
                                        y=df_inv_users_cnts['user_counts'].cumsum().values,
                                        name='All investors'
                                    ),
                                    go.Scatter(
                                        x=df_inv_users_cnts_seed['user_counts'].cumsum().index,
                                        y=df_inv_users_cnts_seed['user_counts'].cumsum().values,
                                        name='Seed investors'
                                    ),
                                    go.Scatter(
                                        x=df_inv_users_cnts_spark['user_counts'].cumsum().index,
                                        y=df_inv_users_cnts_spark['user_counts'].cumsum().values,
                                        name='Spark investors'
                                    )
                                ],
                            },
                            config={"displayModeBar": False}
                        ),
                    ]),
                    # Investor profile by metric
                    html.Div([
                        html.H6("Investor Profiles by Metric", className="subtitle padded"),
                    ],
                        className="row",
                        style={"margin-bottom": "35px"},
                    ),
                    html.P("Please select a metric:"),
                    html.Div([
                        dcc.Dropdown(id='dd-user-metric',
                                     value='User Count #',
                                     options=['User Count #', 'Total Balance $', 'Total Deposit $',
                                              'Total Withdrawal $', 'Total D/W Ratio', 'Avg Deposit $',
                                              'Avg Withdrawal $'],
                                     clearable=False
                                     ),
                        html.Div([
                            html.Div([
                                dcc.Graph(id="pie-user-gender"),
                            ], className="six columns"),
                            html.Div([
                                dcc.Graph(id="pie-user-loc"),
                            ], className="six columns"),
                        ], className="row"),

                        html.Div([
                            html.Div([
                                dcc.Graph(id="bar-user-bal"),
                            ], className="six columns"),
                            html.Div([
                                dcc.Graph(id="bar-user-inv-freq"),
                            ], className="six columns"),
                        ], className="row"),
                    ])
                ],
                className="sub_page",
            ),
        ],
        className="page",
    )