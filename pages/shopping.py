from dash import html
from util.dash_utils import Header
from dash.dash_table import DataTable, FormatTemplate


def create_layout(app, ts, df_user_ss_d, df_merchant_store_ss_d, df_shopping_pmt_by_store):
    money = FormatTemplate.money(0)
    percentage = FormatTemplate.percentage(0)

    # current FUM
    df = df_shopping_pmt_by_store[df_shopping_pmt_by_store['Total Sales $'] > 0].reset_index()
    cols = df.columns
    columns1 = [
        dict(id=cols[0], name='Store ID', type='text'),
        dict(id=cols[1], name='Store Name', type='text'),
        dict(id=cols[2], name=cols[2], type='numeric'),
        dict(id=cols[3], name=cols[3], type='numeric'),
        dict(id=cols[4], name=cols[4], type='numeric', format=money),
        dict(id=cols[5], name=cols[5], type='numeric'),
        dict(id=cols[6], name=cols[6], type='numeric', format=money),
        dict(id=cols[7], name=cols[7], type='numeric', format=money),
        dict(id=cols[8], name=cols[8], type='numeric', format=percentage),
    ]
    data1 = df[cols].to_dict('records')

    return html.Div(
        [
            Header(app, ts),
            # Page 5
            html.Div(
                [
                    # Row 1 - Totals
                    html.Div(
                        [
                            html.Div(
                                [
                                    html.H6(
                                        ["Total Customers"]
                                    ),
                                    html.H6(
                                        [df_user_ss_d[df_user_ss_d['groupbuy_order_amt'] > 0].shape[0]]
                                    ),
                                ],
                                className="three columns",
                            ),
                            # html.Div(
                            #     [
                            #         html.H6(
                            #             ["Total Merchant Stores"]
                            #         ),
                            #         html.H6(
                            #             [df_merchant_store_ss_d.shape[0]]
                            #         ),
                            #     ],
                            #     className="three columns",
                            # ),
                            html.Div(
                                [
                                    html.H6(
                                        ["Total Sales"]
                                    ),
                                    html.H6(
                                        [f'${df_user_ss_d["groupbuy_order_amt"].sum():,.0f}']
                                    ),
                                ],
                                className="three columns",
                            ),
                            html.Div(
                                [
                                    html.H6(
                                        ["Total T-Points Rewarded"]
                                    ),
                                    html.H6(
                                        [f'{df_user_ss_d["groupbuy_t_points"].sum():,.0f}']
                                    ),
                                ],
                                className="three columns",
                            ),
                            html.Div(
                                [
                                    html.H6(
                                        ["Avg. Profit Margin"]
                                    ),
                                    html.H6(
                                        [
                                            f'{df_user_ss_d["groupbuy_t_points"].sum() / df_user_ss_d["groupbuy_order_amt"].sum() * 100:,.0f}%', ]
                                    ),
                                ],
                                className="three columns",
                            ),
                        ],
                        className="row",
                        style={"margin-bottom": "35px"},
                    ),
                    # Row 2 & 3
                    html.Div(
                        [
                            html.Div(
                                [
                                    html.Br([]),
                                    html.Div([
                                        html.H6("Shopping Revenue by Store", className="subtitle padded"),
                                    ],
                                        className="row",
                                        style={"margin-bottom": "35px"},
                                    ),
                                    DataTable(
                                        data=data1,
                                        columns=columns1,
                                        style_cell={
                                            'height': 'auto',
                                            'width': 'auto', 'minWidth': '70px',
                                            'whiteSpace': 'normal',
                                            'fontSize': 12,
                                            'font-family': 'sans-serif'
                                        },
                                    ),
                                ],
                                className="twelve columns",
                            )
                        ],
                        className="row ",
                    ),
                ],
                className="sub_page",
            ),
        ],
        className="page",
    )
