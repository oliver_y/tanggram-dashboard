from dash import dcc, html
from util.dash_utils import Header


def create_layout(app, ts):
    return html.Div(
        [
            Header(app, ts),
            # Page 6
            html.Div(
                [
                    # Row 1
                    html.Div(
                        [
                            html.Div(
                                [
                                    html.H6("Search by Tanggram ID", className="subtitle padded"),
                                    html.Br([]),
                                    html.Div(
                                        [
                                            dcc.Input(id='input-tg-id', placeholder='Please Enter a Tanggram ID...',
                                                      type='number', value=0),
                                            html.Button('Search', id='btn-search-tg-id')
                                        ],
                                        style={"color": "#7a7a7a"},
                                    ),
                                    html.Br(),
                                    html.Table(id='tg-id-search-output-container'),
                                ],
                                className="row",
                            ),
                            html.Div(
                                [
                                    html.H6("Search by Transaction ID", className="subtitle padded"),
                                    html.Br([]),
                                    html.Div(
                                        [
                                            dcc.Input(id='input-tran-id', placeholder='Please Enter a transaction ID...',
                                                      type='number', value=0),
                                            html.Button('Search', id='btn-search-tran-id')
                                        ],
                                        style={"color": "#7a7a7a"},
                                    ),
                                    html.Br(),
                                    html.Table(id='tran-id-search-output-container'),
                                ],
                                className="row",
                            ),
                        ],
                        className="row ",
                    )
                ],
                className="sub_page",
            ),
        ],
        className="page",
    )
