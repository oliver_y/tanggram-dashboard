from dash import html
from dash.dash_table import DataTable, FormatTemplate
from util.dash_utils import Header


def create_layout(app, ts, metrics, df_weekly_summary):

    money = FormatTemplate.money(0)

    # Seed weekly changes
    df = df_weekly_summary.reset_index()
    df['Week End'] = df['Week End'].dt.date
    cols = ['Week End', 'Seed - New Investors', 'Seed - Deposit (completed) $', 'Seed - Deposit (created) $',
            'Seed - Deposit (created) #', 'Seed - Withdrawal (completed) $', 'Seed - Withdrawal (created) $',
            'Seed - Withdrawal (created) #']
    columns1 = [
        dict(id=cols[0], name=cols[0], type='text'),
        dict(id=cols[1], name=cols[1], type='numeric'),
        dict(id=cols[2], name=cols[2], type='numeric', format=money),
        dict(id=cols[3], name=cols[3], type='numeric', format=money),
        dict(id=cols[4], name=cols[4], type='numeric'),
        dict(id=cols[5], name=cols[5], type='numeric', format=money),
        dict(id=cols[6], name=cols[6], type='numeric', format=money),
        dict(id=cols[7], name=cols[7], type='numeric'),
    ]
    data1 = df[cols].to_dict('records')

    # Spark weekly changes
    cols = ['Week End', 'Spark - New Investors', 'Spark - Deposit (completed) $', 'Spark - Deposit (created) $',
            'Spark - Deposit (created) #', 'Spark - Withdrawal (completed) $', 'Spark - Withdrawal (created) $',
            'Spark - Withdrawal (created) #']
    columns2 = [
        dict(id=cols[0], name=cols[0], type='text'),
        dict(id=cols[1], name=cols[1], type='numeric'),
        dict(id=cols[2], name=cols[2], type='numeric', format=money),
        dict(id=cols[3], name=cols[3], type='numeric', format=money),
        dict(id=cols[4], name=cols[4], type='numeric'),
        dict(id=cols[5], name=cols[5], type='numeric', format=money),
        dict(id=cols[6], name=cols[6], type='numeric', format=money),
        dict(id=cols[7], name=cols[7], type='numeric'),
    ]
    data2 = df[cols].to_dict('records')

    return html.Div(
        [
            html.Div([Header(app, ts)]),
            # Page 1
            html.Div(
                [
                    # Totals
                    html.Div(
                        [

                            html.Div(
                                [
                                    html.H6(
                                        ["Total Subscribed Users"]
                                    ),
                                    html.H6(
                                        [f'{71253:,.0f}']
                                    ),
                                ],
                                className="three columns",
                            ),
                            html.Div(
                                [
                                    html.H6(
                                        ["Total Registered Users"]
                                    ),
                                    html.H6(
                                        [f'{metrics["curr_reg_user"]:,.0f}']
                                    ),
                                ],
                                className="three columns",
                            ),
                            html.Div(
                                [
                                    html.H6(
                                        ["Current Investment Users"]
                                    ),
                                    html.H6(
                                        [f'{metrics["curr_inv_user"]:,.0f}']
                                    ),
                                ],
                                className="three columns",
                            ),
                            html.Div(
                                [
                                    html.H6(
                                        ["Current FUM"]
                                    ),
                                    html.H6(
                                        [f'${metrics["curr_fum"]:,.0f}']
                                    ),
                                ],
                                className="three columns",
                            ),
                        ],
                        className="row",
                        style={"margin-bottom": "35px"},
                    ),
                    # Weekly change summary
                    html.Div(
                        [
                            html.Div(
                                [
                                    html.H6(
                                        ["Weekly Change Summary"], className="subtitle padded"
                                    ),
                                    DataTable(
                                        data=data1,
                                        columns=columns1,
                                        style_cell={
                                            'height': 'auto',
                                            'width': 'auto', 'minWidth': '90px',
                                            'whiteSpace': 'normal',
                                            'fontSize': 12,
                                            'font-family': 'sans-serif'
                                        },
                                    ),
                                    html.Br(),
                                    DataTable(
                                        data=data2,
                                        columns=columns2,
                                        style_cell={
                                            'height': 'auto',
                                            'width': 'auto', 'minWidth': '90px',
                                            'whiteSpace': 'normal',
                                            'fontSize': 12,
                                            'font-family': 'sans-serif'
                                        },
                                    ),
                                ],
                                className="six columns",
                            ),
                        ],
                        className="row",
                        style={"margin-bottom": "35px"},
                    ),
                ],
                className="sub_page",
            ),
        ],
        className="page",
    )
